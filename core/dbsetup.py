from uuid import uuid4
from sqlalchemy.orm import relationship
from sqlalchemy_utils import UUIDType, Timestamp
from sqlalchemy import MetaData
from sqlalchemy import  Column, ForeignKey, Integer, String,Boolean ,Float , ForeignKey  , BigInteger ,TIMESTAMP ,Enum ,Sequence , BOOLEAN
from core.extensions import  base as Base


#from app.data.models import Autos

from app.data.usuarios import Usuario
from app.data.grupo import Grupo
from app.data.usuario_grupo import UsuarioGrupo

from app.data.grupo_permiso import GrupoPermiso
from app.data.permiso import Permiso
from app.data.usuario_permiso import UsuarioPermiso

from app.data.colaborador import Colaborador
from app.data.tipo_archivos import TipoArchivo
from app.data.biblioteca_archivos import Biblioteca


from app.data.artist.base_artist import Artists
from app.data.album.base_album import Albums
from app.data.song.base_songs import Songs
from app.data.lyrics.base_lyrics import Lyrics

##alembic revision --autogenerate -m "comentario"
## para ejecutar cambios al cambio mas reciente
##  alembic upgrade head

### LLAVE SECRETA
from dotenv import load_dotenv 
from pathlib import Path
import os
env_path = Path('.') / '.env'

load_dotenv(dotenv_path=env_path, override=True)


SECRET_KEY =os.getenv("SECRET_KEY")

envsettings = os.getenv("ENV")

ALGORITHM = os.getenv("ALGORITHM")
ACCESS_TOKEN_EXPIRE_MINUTES = int ( os.getenv("ACCESS_TOKEN_EXPIRE_MINUTES")  ) 
REFRESH_TOKEN_EXPIRE_MINUTES =  int ( os.getenv("REFRESH_TOKEN_EXPIRE_MINUTES")  ) 

DB_USER = os.getenv("DB_USER")
DB_PASSWORD = os.getenv("DB_PASSWORD")
DB_HOST = os.getenv("DB_HOST")
DB_PORT = os.getenv("DB_PORT")
DB_NAME = os.getenv("DB_NAME")
INCLUDE_SCHEMA = os.getenv("INCLUDE_SCHEMA")
BYPASS_VALIDATION = True if os.getenv("BYPASS_VALIDATION" ,"").upper()=="TRUE" else False
BIBLIOTECA = os.getenv("BIBLIOTECA")
SECURE_DOWNLOAD_KEY=os.getenv("SECURE_DOWNLOAD_KEY","text")
DOWNLOAD_EXPIRES=int ( os.getenv("DOWNLOAD_EXPIRES",300) ) 
RUTA_BANNER = os.getenv("RUTA_BANNER")
#print ( BYPASS_VALIDATION ,"<<<<<<<<<<"  , type(BYPASS_VALIDATION ))
 

LDAP_AUTH_ACTIVE_DIRECTORY_DOMAIN = os.getenv("LDAP_AUTH_ACTIVE_DIRECTORY_DOMAIN")
LDAP_AUTH_URL = os.getenv("LDAP_AUTH_URL")
LDAP_AUTH_CONNECT_TIMEOUT = os.getenv("LDAP_AUTH_CONNECT_TIMEOUT")
LDAP_AUTH_AUTHENTICATION = os.getenv("LDAP_AUTH_AUTHENTICATION")
LDAP_AUTH_RECEIVE_TIMEOUT = os.getenv("LDAP_AUTH_RECEIVE_TIMEOUT")
LDAP_AUTH_USE_TLS = os.getenv("LDAP_AUTH_USE_TLS")
LDAP_AUTH_SEARCH_BASE = os.getenv("LDAP_AUTH_SEARCH_BASE")

from starlette.config import Config
from starlette.datastructures import Secret,URL
from core.settings.settings import BaseConfig
import core.config as core_config

class DevSettings(BaseConfig):

    """ Configuration class for site development environment """

    config = Config()

    DEBUG = config("DEBUG", cast=bool, default=True)

    DB_USER = core_config.DB_USER
    DB_PASSWORD = core_config.DB_PASSWORD
    DB_HOST = core_config.DB_HOST
    DB_PORT = core_config.DB_PORT
    DB_NAME = core_config.DB_NAME
    INCLUDE_SCHEMA = core_config.INCLUDE_SCHEMA
    
    
    
    DATABASE_URL = config(
        "DATABASE_URL",
        default=f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}",
        
        #default = f"oracle+cx_oracle://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:1521/?service_name={DB_NAME}"


        
    )
    def imprimir (self):

        print ( 
                {
                "DEBUG": self.DEBUG,
                "DB_USER": self.DB_USER,
                "DB_PASSWORD": self.DB_PASSWORD,
                "DB_HOST": self.DB_HOST,
                "DB_PORT": self.DB_PORT,
                "DB_NAME": self.DB_NAME,
                "INCLUDE_SCHEMA": self.INCLUDE_SCHEMA,
                    "default":f"postgresql://{self.DB_USER}:{self.DB_PASSWORD}@{self.DB_HOST}:{self.DB_PORT}/{self.DB_NAME}",
                }

        )
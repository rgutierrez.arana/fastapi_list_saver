
from app.api.v1.auth.schemas import User


class BaseStorage:
    def __init__(self,
        db,

    ) -> None:
        self.db = db

class BaseUseCase:

    def __init__(self,
        db,
        current_user : User,
        storage:BaseStorage
    ) -> None: 
        self.db = db
        self.current_user:User = current_user
        self.storage = storage(db=self.db)




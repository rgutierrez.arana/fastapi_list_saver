from sqlalchemy.ext.declarative import declarative_base
# # from core.factories import Session
from sqlalchemy import MetaData
from sqlalchemy.engine import create_engine
from core.factories import settings
from sqlalchemy.orm import sessionmaker
from sqlalchemy import  Column, ForeignKey, Integer, String,Boolean ,Float , ForeignKey ,BigInteger,  TIMESTAMP
from sqlalchemy.orm import scoped_session
from contextlib import contextmanager

from sqlalchemy.orm import Session

engine = create_engine(settings.DATABASE_URL , max_identifier_length = 128 , connect_args={"check_same_thread": False})

convention = {
    "ix": "ix_%(column_0_label)s",
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s",
}
metadata = MetaData()



base = declarative_base(bind = engine , metadata=metadata )

Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)

session = scoped_session(Session)
def get_session()->scoped_session:
    
    return  session
def get_db()->Session:
    db = Session()
    try:
        yield db
    finally:
        db.close()

class SessionManager(object):
    def __init__(self, db_session ):
        self.db_session = db_session
    def __enter__(self):
        return self.db_session
    def __exit__(self, type, value, traceback):
        
        self.db_session.remove()

from datetime import datetime
from calendar import timegm
from fastapi import HTTPException , status

from fastapi.param_functions import Form
from typing import Optional ,List, Dict
from enum import Enum

from fastapi.openapi.models import OAuth2 as OAuth2Model, OAuthFlows as OAuthFlowsModel
from fastapi.security import OAuth2
from starlette.requests import Request
from fastapi.security.utils import get_authorization_scheme_param
from fastapi.exceptions import HTTPException
from starlette.status import HTTP_401_UNAUTHORIZED, HTTP_403_FORBIDDEN

from fastapi.openapi.models import OAuth2 as OAuth2Model, OAuthFlows as OAuthFlowsModel
from core.config import BYPASS_VALIDATION
def validate_expired(exp: int, leeway=0):

    now = timegm(datetime.utcnow().utctimetuple())
    print ( exp - now  )
    
    if exp < (now - leeway):
        raise HTTPException(status_code=412,
                            detail={"msg":"REFRESH-REQUIRED" , "sub-code":101},
                            headers={"WWW-Authenticate": "Bearer"}, )



class GrantType(str,Enum):
    ldap="ldap"
    password="password"
    apikey="apikey"
class RequestFormAccess:

    def __init__(
        self,
        grant_type: GrantType = Form(None),
        username: str = Form(""),
        password: str = Form(""),
        scope: str = Form(""),
        client_id: Optional[str] = Form(None),
        client_secret: Optional[str] = Form(None),
        apikey: Optional[str] = Form(None),
    ):
        self.grant_type = grant_type
        self.username = username
        self.password = password
        self.apikey = apikey
        self.scopes = scope.split()
        self.client_id = client_id
        self.client_secret = client_secret



class OAuth2PasswordBearerMod(OAuth2):
    def __init__(
        self,
        tokenUrl: str,
        scheme_name: str = None,
        scopes: dict = None,
        auto_error: bool = True,
    ):
        if not scopes:
            scopes = {}
        flows = OAuthFlowsModel(password={"tokenUrl": tokenUrl, "scopes": scopes})
        super().__init__(flows=flows, scheme_name=scheme_name, auto_error=auto_error)

    async def __call__(self, request: Request) -> Optional[str]:
        if BYPASS_VALIDATION:
            return True
        authorization: str = request.headers.get("Authorization")
        scheme, param = get_authorization_scheme_param(authorization)
        if not authorization or scheme.lower() != "bearer":
            if self.auto_error:
                raise HTTPException(
                    status_code=HTTP_401_UNAUTHORIZED,
                    detail="Not authenticated",
                    headers={"WWW-Authenticate": "Bearer"},
                )
            else:
                return None
        return param
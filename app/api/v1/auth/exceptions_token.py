from fastapi import HTTPException , status

unauthorized = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Usuario o password invalido",
    headers={"WWW-Authenticate": "Bearer"},
)

credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="No podemos validar las credenciales",
        headers={"WWW-Authenticate": "Bearer"},
    )

type_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="No hay tipo definido en el token ingresado",
        headers={"WWW-Authenticate": "Bearer"},
    )

token_type_refresh_exception  = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Token invalido de refresco para el acceso",
        headers={"WWW-Authenticate": "Bearer"},
    )

ldap_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="No podemos validar las credenciales contra el LDAP",
        headers={"WWW-Authenticate": "Bearer"},
    )

type_login_exception= HTTPException(
        status_code=422,
        detail="El tipo de login solicitado no es valido",
        headers={"WWW-Authenticate": "Bearer"},
    )

def falta_permiso(permiso:list = []):
    return HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail={"msg" :"El usuario actual no tiene acceso a esta funcionalidad"  , "codigo" :"FALTA_PERMISO " , "permisos" : permiso },
        headers={"WWW-Authenticate": "Bearer"},   
)
permision_exception=HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail={"msg" :"El usuario actual no tiene acceso a esta funcionalidad"  , "codigo" :"FALTA_PERMISO" },
        headers={"WWW-Authenticate": "Bearer"},   
)
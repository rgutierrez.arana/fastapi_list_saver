from datetime import datetime, timedelta
from typing import Optional

from fastapi import (APIRouter, Body, Cookie, Depends, FastAPI, File, Header,
                     HTTPException, Query, status, Response)

from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
from passlib.context import CryptContext
from requests.sessions import session

from core.config import ACCESS_TOKEN_EXPIRE_MINUTES, ALGORITHM, SECRET_KEY ,REFRESH_TOKEN_EXPIRE_MINUTES,BYPASS_VALIDATION

from .schemas import *
from core.dbsetup import Usuario , UsuarioPermiso , Grupo , UsuarioGrupo ,Permiso,GrupoPermiso
from core.dbsetup import Colaborador


from core.extensions import get_db, get_session , SessionManager
import sqlalchemy.exc as exc
from sqlalchemy.sql import expression, functions , literal ,cast
from sqlalchemy import or_,String
from app.utils.error_responses import error_msg
from .exceptions_token import unauthorized ,credentials_exception ,type_exception ,type_login_exception,token_type_refresh_exception
from .utils import validate_expired , RequestFormAccess,OAuth2PasswordBearerMod
from app.utils.ldap_util import BaseLdap
import random
#0ec0338349ec376a2d02753199574ef107ee6b587ff078907839786e034ae7ed
# to get a string like this run:
# openssl rand -hex 32

tags=["Auth"] 





pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
# if BYPASS_VALIDATION:
oauth2_scheme = OAuth2PasswordBearerMod(tokenUrl="/api/v1.0/auth/token")
# else:
#     oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/api/v1.0/auth/token")

router = APIRouter()


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)


##########################################################################
def get_user(db, username: str):
    
    if username in db:
        user_dict = db[username]
        return UserInDB(**user_dict)


def authenticate_user(fake_db, username: str, password: str):
    user = get_user(fake_db, username)
    if not user:
        return False
    if not verify_password(password, user.hashed_password):
        return False
    return user
##########################################################################
def get_user_db(username: str , id_user :int , session = Depends(get_db)):

    
    
    try:
        
        usuario_actual=session.query( 
            Usuario 
            ).filter( 
                Usuario.id == id_user ,
                Usuario.visible == 1
            ).first()
        if usuario_actual:
            return usuario_actual
        else : 
            return False
    except Exception as ex:
        import traceback
        excp = traceback.format_exc()

        raise HTTPException(status_code=500 , detail=ex.args)


def is_user_in_db(username: str , mail:str):
    try : 
        session:any=get_session() 
        usuario_actual=session.query( 
            Usuario 
            ).filter( 
                or_(Usuario.username ,Usuario.correo== mail)
                
            ).first()
        if usuario_actual:
            return True
        else : 
            return False
    except Exception as ex:
        session.remove()
        raise HTTPException(status_code=500 , detail=ex.args)
    finally :
        session.remove()
    # if username in db:
    #     user_dict = db[username]
    #     return UserInDB(**user_dict)

def authenticate_user_db( username:str="",password:str="" ,type_access:str ="web"  ,db = get_session()  )->Usuario:
    try :
        session:any=db
        usuarios_actuales:List[Usuario]=session.query( 
            Usuario 
            ).join(
                Colaborador
            ).filter( 
                or_(Usuario.username == username , Usuario.correo == username) ,
                Usuario.visible ==True,            
            ).with_entities(
                Usuario.id,
                Usuario.username,
                Usuario.hash_password,
                Usuario.correo,
         
                (Colaborador.nombre +" "+Colaborador.apellido ).label("nombre_completo") 
            ).all()

        if type_access =="web":

            if usuarios_actuales:
                for usuario in usuarios_actuales:
                    if Usuario.check_password(password=password , hashed_passwd= usuario.hash_password):
                        return usuario
                return False
            
            else :
                False
        elif  type_access =="ldap":

            if usuarios_actuales:
                for usuario in usuarios_actuales:
                    return usuario
                    
                return False
            
            else :
                False
        
        else:
            raise HTTPException(status_code=500 , detail={"msg":"Error en el metodo de acceso solo pueden ser [web ,ldap] "})
    except Exception as ex:
        raise HTTPException(status_code=500 , detail=ex.args)
    # finally :
    #     session.remove()
def authenticate_device(apikey:str = None):

    with SessionManager(get_session() )as db:
        print ( apikey)
        user_api_key=db.query(Usuario).filter(Usuario.apikey == apikey).with_entities(
                Usuario.id,
                Usuario.username,
                ("Device-" + Usuario.id.cast(String) ).label("nombre_completo"),
                Usuario.correo
                ).first()
        if user_api_key:
            return user_api_key
        else:
            return False


def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire , "type" :"access"})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt
def create_refresh_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=2)
    to_encode.update({"exp": expire , "type" : "refresh"})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt

async def get_current_user(token: str = Depends(oauth2_scheme) , db_session = Depends(get_db) ):
    if BYPASS_VALIDATION:
        return True
    try:


        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM] , options={"verify_exp":False})
        username: str = payload.get("sub")
        token_type:str = payload.get("type",None)
        id_user:str = payload.get("id_user",None)
        exp = payload.get("exp")

        if username is None:
            raise credentials_exception
        if token_type!="access":
            raise token_type_refresh_exception
        if token_type==None:
            raise type_exception
        validate_expired(exp = exp )        
        token_data = TokenData(username=username,id_user=id_user)
    except JWTError:
        raise credentials_exception

    user = get_user_db( username=token_data.username , id_user=token_data.id_user, session= db_session)
    if user is None:
        raise credentials_exception
    return user , db_session

from typing import Tuple

async def get_current_active_user(user_session: Tuple[User,"session" ] = Depends(get_current_user) )-> Tuple[User,"session" ]:
    print(BYPASS_VALIDATION)

    current_user , session = user_session
    if BYPASS_VALIDATION:
        return User(
            id=122,
            username="ad",
            correo="ad@example.com"
        )

    if not current_user.visible:
        raise HTTPException(status_code=400, detail="Inactive user")
    
    return current_user, session


@router.post("/token", response_model=Token, tags=tags)
async def login_for_access_token(form_data: RequestFormAccess = Depends() , db_session = Depends(get_db) ):
    if form_data.grant_type == "password":
        print (form_data.username , form_data.password)
        user = authenticate_user_db(username= form_data.username,password= form_data.password,type_access="web" , db=db_session)

        

        if not user:
            raise unauthorized

        access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES  )
        access_token = create_access_token(
            data={  "sub": user.username ,
                    "id_user" : user.id ,
                    "generated" : str(datetime.now()) 
                }, expires_delta=access_token_expires
            )

        refresh_token_expires = timedelta(minutes=REFRESH_TOKEN_EXPIRE_MINUTES )
        refresh_token = create_refresh_token(
            data={  "sub": user.username ,
                    "id_user" : user.id ,
                    "generated":str(datetime.now() )
                }, expires_delta=refresh_token_expires
            )
        
        return {
                "id": user.id,
                "correo": user.correo,
                "access_token": access_token,
                "refresh_token": refresh_token,
                "token_type": "bearer",
                "nombre_completo": user.nombre_completo,
            }
    elif form_data.grant_type == "apikey":

        user=authenticate_device(apikey=form_data.apikey)
        if not user:
            raise unauthorized

        access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES  )
        access_token = create_access_token(
            data={  "sub": user.username ,
                    "id_user" : user.id ,
                    "generated" : str(datetime.now()) 
                }, expires_delta=access_token_expires
            )

        refresh_token_expires = timedelta(minutes=REFRESH_TOKEN_EXPIRE_MINUTES )
        refresh_token = create_refresh_token(
            data={  "sub": user.username ,
                    "id_user" : user.id ,
                    "generated":str(datetime.now() )
                }, expires_delta=refresh_token_expires
            )
        
        return {
                "id": user.id,
                "correo": user.correo,
                "access_token": access_token,
                "refresh_token": refresh_token,
                "token_type": "bearer",
                "nombre_completo": user.nombre_completo,
            }       

    elif form_data.grant_type == "ldap" :
        ldap_conn=BaseLdap(username=form_data.username ,password= form_data.password)
        data_ldap=ldap_conn.conectar()
        if data_ldap ==None:
            raise credentials_exception

        user = authenticate_user_db(username= form_data.username,password= form_data.password,type_access="ldap")
        if user in (False, None):
            print("Usuario no registrado")

            #TODO  obtener datos del usuario del ldap y registrarlo
            
        else:

            access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES + random.randint(5,15)  )
            access_token = create_access_token(
                data={  "sub": user.username ,
                        "id_user" : user.id ,
                        "generated" : str(datetime.now()) 
                    }, expires_delta=access_token_expires
                )

            refresh_token_expires = timedelta(minutes=REFRESH_TOKEN_EXPIRE_MINUTES + random.randint(20,30))
            refresh_token = create_refresh_token(
                data={  "sub": user.username ,
                        "id_user" : user.id ,
                        "generated":str(datetime.now() )
                     }, expires_delta=refresh_token_expires
                )
            
            return {
                    "id": user.id,
                    "correo": user.correo,
                    "access_token": access_token,
                    "refresh_token": refresh_token,
                    "token_type": "bearer",
                    "nombre_completo": user.nombre_completo,
             
                 
                }


        #raise HTTPException(status_code=500 , detail={"TODO" : "falta integrar LDAP en pruebas"})

    else:
        raise type_login_exception

@router.post("/get-token", response_model=RevalidateToken, tags=tags)
async def get_new_access_token(token : RefreshToken ):
    try :
        payload = jwt.decode(token.token, SECRET_KEY, algorithms=[ALGORITHM] )

        username: str = payload.get("sub")
        token_type:str = payload.get("type",None)
        id_user:int  = payload.get("id_user",None)
        
        exp = payload.get("exp")
        nombre = payload.get("sub")
        if token_type ==None:
            raise type_exception
        if token_type!="refresh":
            raise token_type_refresh_exception
        if username is None:
            raise credentials_exception


        access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
        access_token = create_access_token(
            data={"sub": nombre , "id_user" :id_user  , "generated" : str(datetime.now() ) }, expires_delta=access_token_expires
        )

        return {"access_token": access_token, "token_type": "bearer"}
    except JWTError:
        raise credentials_exception


# @router.post("/test-get", response_model=RevalidateToken, tags=tags)
# async def get_new_access_token(token : RefreshToken ,
#     current_user = Depends(get_current_user)
#     ):
#     try :
#         payload = jwt.decode(token.token, SECRET_KEY, algorithms=[ALGORITHM] )

#         username: str = payload.get("sub")
#         token_type:str = payload.get("type",None)
#         id_user:int  = payload.get("id_user",None)
        
#         exp = payload.get("exp")
#         nombre = payload.get("sub")
#         if token_type ==None:
#             raise type_exception
#         if token_type!="refresh":
#             raise token_type_refresh_exception
#         if username is None:
#             raise credentials_exception


#         access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
#         access_token = create_access_token(
#             data={"sub": nombre , "id_user" :id_user  , "generated" : str(datetime.now() ) }, expires_delta=access_token_expires
#         )

#         return {"access_token": access_token, "token_type": "bearer"}
#     except JWTError:
#         raise credentials_exception


# @router.get("/users/me/items/" , tags = tags)
# async def read_own_items(current_user: User = Depends(get_current_active_user)):
#     return [{"item_id": "Foo", "owner": current_user.nombre}]

# @router.post("/users/ingresar-usuario" , tags=tags)
# def ingresar_usuario_nuevo (  
#                             new_user:NewUser,
#                             db  = Depends(get_session),
#                             #cur_user:User= Depends(get_current_active_user)
#                              ):
#     try : 
        
#         usuario_nuevo = Usuario(**new_user.dict() )
#         db.add(usuario_nuevo)
#         db.commit()
#         return new_user.dict()

#     except exc.SQLAlchemyError as sqlerror:
#         import traceback as tb
#         error_data=error_msg(error_code= sqlerror.code )
#         raise HTTPException(status_code=400 ,detail= error_data)

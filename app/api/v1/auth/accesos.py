from typing import Optional ,List, Dict
from fastapi import Depends
from core.extensions import get_session
from app.api.v1.auth.schemas import User
from core.dbsetup import UsuarioGrupo,Grupo,GrupoPermiso,Permiso,UsuarioPermiso,Usuario
from app.api.v1.auth.exceptions_token import permision_exception ,falta_permiso
from app.api.v1.auth.controller import get_current_active_user 
from core.config import BYPASS_VALIDATION

def validar_acceso(
    accesos: List[str] = [],
    current_user: User = None,
    db=get_session() ):
    if BYPASS_VALIDATION:
        return {"permisos_usuario": accesos, "permisos_grupo": []}

    permisos_usuario = db.query(
        Usuario
    ).join(
        UsuarioPermiso
    ).join(
        Permiso
    ).filter(
        #Usuario.visible == 1,
        UsuarioPermiso.activo == 1,
        Usuario.id == current_user.id,
        Permiso.codigo.in_(accesos),
    ).with_entities(
        Permiso.codigo,
        Permiso.id,
    ).all()

    permisos_grupo = db.query(
        UsuarioGrupo
    ).join(
        Usuario,
        Usuario.id == UsuarioGrupo.id_usuario
    ).join(
        Grupo
    ).join(
        GrupoPermiso
    ).join(
        Permiso
    ).filter(
        UsuarioGrupo.id_usuario == current_user.id,
        #Usuario.visible == 1,
        Permiso.activo == 1,
        Grupo.activo == 1,
        Permiso.codigo.in_(accesos)
    ).with_entities(
        Permiso.codigo,
        Permiso.id,
    ).all()

    if permisos_grupo == [] and permisos_usuario == []:

        raise falta_permiso(permiso=accesos)

    return {"permisos_usuario": permisos_usuario, "permisos_grupo": permisos_grupo}


    
    ### primero le doy al del usuario




        

        

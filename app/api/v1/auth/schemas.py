from app.utils.general_schemas import * 
class Token(OrmModel):
    id:int
    access_token: str
    token_type: str
    refresh_token :str
    correo:str
    nombre_completo:str

class RevalidateToken(OrmModel):
    access_token:str
    token_type:str

class TokenData(OrmModel):
    username: Optional[str] = None
    id_user:Optional[int]=None


class User(OrmModel):
    id : int
    username: str
    correo: Optional[str] = None

class NewUser(OrmModel):
    username:str
    correo:EmailStr
    password:str



class UserInDB(User):
    hashed_password: str

class RefreshToken(OrmModel):
    token:str


class PermisoUsuario(OrmModel):
    codigo:str
    id:int
class PermisosGrupo(OrmModel):
    codigo:str
    id:int

class PermisosUsuarios(OrmModel):
    permisos_usuario:List[PermisoUsuario] = []
    permisos_grupo:List[PermisoUsuario] = []
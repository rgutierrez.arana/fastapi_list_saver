from typing import List
from core.base_hexagonal import BaseUseCase,BaseStorage
from core.dbsetup import Albums,Songs , Artists
from app.api.v1.albums.schemas import PagerAlbums

from sqlalchemy.exc import IntegrityError
from fastapi.exceptions import HTTPException

class AlbumStorage(BaseStorage):
    

    def paginated_albums(
        self,
        item_pagina:int ,
        pag_actual:int ,
        tipo_filtro:str ="",
        param_filtro:str ="",
    )->PagerAlbums:

        db_sentence: List[Albums] = self.db.query(
            Albums
        ).join(
            Artists,
            Artists.id == Albums.id_artista
        ).with_entities(
            Albums.id,
            Albums.name,
            Albums.id_artista,
            Artists.name.label("artist_name")
        )


        current_pager = PagerAlbums(
            item_pagina= item_pagina,
            pag_actual=pag_actual,
            
        )

        current_pager.paginate(db_sentence=db_sentence)


        return current_pager

    def create_album(
        self,
        name:str ,
        id_artist:int = None
    )->Albums:
        new_album=Albums(
            name = name,
            id_artista = id_artist
        )

        try:
            self.db.add(new_album)

            self.db.commit()
            return new_album
        except IntegrityError as ex:
            raise HTTPException(
                status_code=400,
                detail=ex.args
            ) 

    def detail_album(
        self,
        id_album: int,
    ) -> Albums:

        return self.db.query(
            Albums
        
        ).filter(
            Albums.id == id_album
        ).first()


    def detail_album_songs(
        self,
        id_album: int,
    ):
        current_album = self.detail_album(id_album=id_album)

        album_songs=self.db.query(
            Songs
        ).filter(
            Songs.album_id==current_album.id
        ).all()

        return {
            "id" : current_album.id,
            "name" : current_album.name,
            "album_songs" : album_songs
        }


from fastapi import (APIRouter, Body, Cookie, Depends, FastAPI, File, Header,
                     HTTPException, Query, status, Response)



from app.api.v1.albums.schemas import *
from app.api.v1.albums.use_cases import AlbumListUc,AlbumUc, AlbumDetailUc

tags=["ALBUMS"] 
router = APIRouter()


@router.get("", response_model=PagerAlbums, tags=tags)
async def get_albums( 
    
    elemento:AlbumListUc = Depends(AlbumListUc)

):

    return elemento.album_paginated()
    

@router.post("" , response_model=NewAlbumOut , tags=tags)
def create_album(
    use_case:AlbumUc =Depends(AlbumUc)
):
    return use_case.create()



@router.get("/{id_album}" , response_model=NewAlbumOut , tags=tags)
def retrieve_album(
    use_case:AlbumDetailUc =Depends(AlbumDetailUc)
):
    retrieved =use_case.retrieve()
    return retrieved

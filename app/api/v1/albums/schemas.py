
from typing import List, Optional
from app.utils.utils import Search,Pager,OrmModel
from dataclasses import dataclass


class RowAlbum(OrmModel):
    id:int 
    name:Optional[str]
    id_artista :Optional[int] =None
    artist_name :str = None
class PagerAlbums(Pager):

    data:List[RowAlbum] = []


class NewAlbum(OrmModel):
    name:str = None
    id_artist:int = None

class AlbumSongs(OrmModel):
    id:int
    name:str

class NewAlbumOut(NewAlbum):
    id:int =None
    id_artista:int = None
    album_songs:List[AlbumSongs] = []
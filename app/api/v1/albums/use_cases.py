

from typing import Tuple
from app.api.v1.albums.schemas import PagerAlbums , NewAlbum
from app.api.v1.auth.controller import get_current_active_user
from app.utils.utils import Search
from fastapi.params import Depends
from app.utils.general_schemas import User
from core.base_hexagonal import BaseUseCase
from app.api.v1.albums.storage import AlbumStorage


class AlbumListUc(BaseUseCase):
    storage :AlbumStorage = None
    def __init__(self,
                 args: Search = Depends(Search),
                 context_user=Depends(get_current_active_user),
                 ) -> None:
        user, db_session = context_user
        self.args:Search = args

        
        super().__init__(
            db=db_session,
            current_user=user,
            storage=AlbumStorage
        )

    def album_paginated(self)->PagerAlbums:

        return self.storage.paginated_albums(
            item_pagina= self.args.item_pagina,
            pag_actual=self.args.pag_actual
        )

class AlbumUc(BaseUseCase):
    storage :AlbumStorage = None
    def __init__(self,
                 args: NewAlbum ,
                 context_user=Depends(get_current_active_user),
                 ) -> None:
        user, db_session = context_user

        self.args:NewAlbum = args

        
        super().__init__(
            db=db_session,
            current_user=user,
            storage=AlbumStorage
        )

    def create(self):
        return self.storage.create_album(name=self.args.name , id_artist= self.args.id_artist)



class AlbumDetailUc(BaseUseCase):
    storage :AlbumStorage = None
    def __init__(self,
                 id_album:int,
                 context_user=Depends(get_current_active_user),
                 ) -> None:
        user, db_session = context_user

        self.id_album:int = id_album

        
        super().__init__(
            db=db_session,
            current_user=user,
            storage=AlbumStorage
        )

    def retrieve(self):
        current_resp =self.storage.detail_album_songs(id_album=self.id_album)
        
        return current_resp

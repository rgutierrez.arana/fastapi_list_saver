from core.dbsetup import Lyrics
from app.api.v1.lyrics.schemas import PagerLyrics , RowLyrics
from app.utils.utils import Pager
from typing import List
from core.base_hexagonal import BaseUseCase,BaseStorage
from core.dbsetup import Albums,Songs


from sqlalchemy.exc import IntegrityError
from fastapi.exceptions import HTTPException
import random 

class LyricStorage(BaseStorage):
    

    def paginated(
        self,
        item_pagina:int ,
        pag_actual:int ,
        tipo_filtro:str ="",
        param_filtro:str ="",
    )->PagerLyrics:

        db_sentence:List[Lyrics] = self.db.query(
            Lyrics
        ).join(
            Songs,
            Songs.id == Lyrics.song_id
        ).join(
            Albums,
            Albums.id == Songs.album_id
        ).with_entities(
            Lyrics.id,
            Lyrics.name,
            Lyrics.text,

            Songs.id.label("song_id"),
            Songs.name.label("song_name"),

            Albums.id.label("album_id"),
            Albums.name.label("album_name"),
            
            
        )

        current_pager = PagerLyrics(
            item_pagina= item_pagina,
            pag_actual=pag_actual,   
        )
        current_pager.paginate(db_sentence=db_sentence)

        return current_pager

    def create(
        self,
        name:str ,
        song_id:int,
        text:str,

    )->Lyrics:
        new=Lyrics(
            name = name,
            song_id=song_id,
            text = text
        )

        try:
            self.db.add(new)

            self.db.commit()
            return new
        except IntegrityError as ex:
            raise HTTPException(
                status_code=400,
                detail=ex.args
            ) 

    def detail_lyric(
        self,
        id_lyric: int,
    ) -> Lyrics:

        return self.db.query(
            Lyrics
        
        ).filter(
            Lyrics.id == id_lyric
        ).first()


    def detail(
        self,
        id_lyric: int,
    )->RowLyrics:
        # current_album = self.detail_lyric(id_lyric=id_lyric)

        data=self.db.query(
            Lyrics
        ).join(
            Songs,
            Songs.id== Lyrics.song_id
        ).join(
            Albums,
            Albums.id == Songs.album_id
        ).filter(
            Lyrics.id==id_lyric
        ).with_entities(
            Lyrics.id,
            Lyrics.name,
            Lyrics.text,
            Lyrics.song_id,
            Songs.name.label("song_name"),
            Songs.album_id,
            Songs.name.label("album_name"),
        ).first()

        return data

    def random_detail(
        self,
        
    )->RowLyrics:
        

        data=self.db.query(
            Lyrics
        ).join(
            Songs,
            Songs.id== Lyrics.song_id
        ).join(
            Albums,
            Albums.id == Songs.album_id
        ).with_entities(
            Lyrics.id,
            Lyrics.name,
            Lyrics.text,
            Lyrics.song_id,
            Songs.name.label("song_name"),
            Songs.album_id,
            Songs.name.label("album_name"),
        ).all()

        random_lyric = random.choice(data)

        return random_lyric

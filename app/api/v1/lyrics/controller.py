
from fastapi import (APIRouter, Body, Cookie, Depends, FastAPI, File, Header,
                     HTTPException, Query, status, Response)
from app.api.v1.lyrics.schemas import *
from app.api.v1.lyrics.use_cases import LyricDetailUc,  LyricsUC,NewLyricsUC,LyricRandomlUc

tags=["LYRICS"] 
router = APIRouter()

@router.get("", response_model=PagerLyrics, tags=tags)
async def get_lyrics_list( 
    elemento:LyricsUC = Depends(LyricsUC)
):

    return elemento.album_paginated()

@router.post("" , response_model=NewLyrics , tags=tags)
def create_album(
    use_case:NewLyricsUC =Depends(NewLyricsUC)
):
    return use_case.create()


@router.get("/random" , response_model=RowLyrics , tags=tags)
def retrieve_random_lyric(
    use_case:LyricRandomlUc =Depends(LyricRandomlUc)
):
    retrieved =use_case.retrieve_random()
    return retrieved


@router.get("/{id_lyric}" , response_model=RowLyrics , tags=tags)
def retrieve_detail_lyric(
    use_case:LyricDetailUc =Depends(LyricDetailUc)
):
    retrieved =use_case.retrieve()
    return retrieved



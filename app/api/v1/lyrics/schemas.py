
from typing import List, Optional
from app.utils.utils import Search,Pager,OrmModel
from dataclasses import dataclass


class RowLyrics(OrmModel):
    id: int = None
    name: Optional[str] = None
    text:str = None
    song_id: int = None
    song_name: str = None
    album_id: int = None
    album_name: str = None
    

    
class PagerLyrics(Pager):

    data:List[RowLyrics] = []


class NewLyrics(OrmModel):
    name:str 
    song_id:int
    text:str


class NewLyricsOut(NewLyrics):
    id:int =None
    

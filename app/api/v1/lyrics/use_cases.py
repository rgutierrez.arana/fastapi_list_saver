

from typing import Tuple
from app.api.v1.lyrics.schemas import PagerLyrics , NewLyrics
from app.api.v1.auth.controller import get_current_active_user
from app.utils.utils import Search
from fastapi.params import Depends
from app.utils.general_schemas import User
from core.base_hexagonal import BaseUseCase
from app.api.v1.lyrics.storage import LyricStorage


class LyricsUC(BaseUseCase):
    storage :LyricStorage = None
    def __init__(self,
                 args: Search = Depends(Search),
                 context_user=Depends(get_current_active_user),
                 ) -> None:
        user, db_session = context_user
        self.args:Search = args

        
        super().__init__(
            db=db_session,
            current_user=user,
            storage=LyricStorage
        )

    def album_paginated(self)->PagerLyrics:

        return self.storage.paginated(
            item_pagina= self.args.item_pagina,
            pag_actual=self.args.pag_actual
        )

class NewLyricsUC(BaseUseCase):
    storage :LyricStorage = None
    def __init__(self,
                 args: NewLyrics ,
                 context_user=Depends(get_current_active_user),
                 ) -> None:
        user, db_session = context_user

        self.args:NewLyrics = args

        
        super().__init__(
            db=db_session,
            current_user=user,
            storage=LyricStorage
        )

    def create(self):
        return self.storage.create(**self.args.dict())



class LyricDetailUc(BaseUseCase):
    storage :LyricStorage = None
    def __init__(self,
                 id_lyric:int,
                 context_user=Depends(get_current_active_user),
                 ) -> None:
        user, db_session = context_user

        self.id_lyric:int = id_lyric

        
        super().__init__(
            db=db_session,
            current_user=user,
            storage=LyricStorage
        )

    def retrieve(self):
        current_resp =self.storage.detail(id_lyric=self.id_lyric)
        
        return current_resp


class LyricRandomlUc(BaseUseCase):
    storage :LyricStorage = None
    def __init__(self,
                context_user=Depends(get_current_active_user)
        ) -> None:
        user, db_session = context_user

       

        
        super().__init__(
            db=db_session,
            current_user=user,
            storage=LyricStorage
        )

    def retrieve_random(self):
        current_resp =self.storage.random_detail()
        
        return current_resp

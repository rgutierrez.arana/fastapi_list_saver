from fastapi import APIRouter, HTTPException,Cookie, Depends,Header,File, Body,Query
from starlette.responses import JSONResponse




from core.extensions import get_session
import os
import time

from core.dbsetup import ClasificacionSitios
from .schemas import InTipoSitio ,OutTipoSitio

router = APIRouter()
tags=["TEST"]

@router.post("/tipo-sitio",tags=tags,)

async def test_tipo_sitio(  tipo_sitio:InTipoSitio,
    session=Depends(get_session) ) :

    nuevo_tipo_sitio=ClasificacionSitios(**tipo_sitio.dict())
    session.add(nuevo_tipo_sitio)
    session.commit()

    return JSONResponse({"result" : True})

@router.get("/tipo-sitio",tags=tags,response_model = OutTipoSitio )

async def obtener_tipo_sitio( 
    session=Depends(get_session) ) :

    sitios=session.query( 
        ClasificacionSitios 
        ).filter( 
            ClasificacionSitios.id != None
        ).all()
    data = {"sitios" : sitios}
    return data

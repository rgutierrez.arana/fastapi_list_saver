from .schema import * 

from datetime import datetime, timedelta
from typing import Optional

from fastapi import (APIRouter, Body, Cookie, Depends, FastAPI, File, Header,
                     HTTPException, Query, status, Response,Cookie ,WebSocket, WebSocketDisconnect)
from sqlalchemy.sql import expression, functions , literal ,cast
from sqlalchemy import or_,String
from app.utils.error_responses import error_msg
from .utils import validate_cookie , manager
tags=["WS"] 


router = APIRouter()

@router.post("/device/{id_device}/mensaje" , tags=tags, response_model = StatusSuccess)
async def enviar_mensaje_usuario(
        id_device:int,
        args:WebsocketMsg,):
    try :
        indice_dispositivo = manager.id_usuarios.index(id_device)
        
        dispositivo:WebSocket=manager.active_connections[indice_dispositivo]
        
        await dispositivo.send_json({"msg" : args.msg , "from" :"server"})
        return success

    except :
        
        import traceback as tb
        tb.print_exc()
        raise HTTPException(
            status_code=412,
            detail={
                "msg": f"Error el id_dispositivo {id_device} no esta conectado"}
        )

    return {"status":"success"}

@router.websocket("/device")
async def websocket_endpoint(
            websocket: WebSocket, 
            id_user: int = Depends(validate_cookie)
            ):
    
    
    validado =await manager.connect(websocket=websocket , id_usuario=id_user)
    # print ( manager.active_connections , "<< conecciones activas")
    # print ( manager.id_usuarios , "<< ids_ usuarios")
    if validado ==False:
        return 

    # print ("id_usuario" , id_user , "<<<<") 
    try:
        while True:
            data = await websocket.receive_json()

            if data["action"] == "SERVER_TEST":
                print (data["msg"] , "<-- mensaje desde el usuario ")
            elif data["action"] == "SEND_TO":
                await manager.send_message_between(data={
                                                    "to" : data["to"],
                                                    "msg":data["msg"],
                                                },
                                                ws_origin= websocket
                                                )
            

    except WebSocketDisconnect:
        manager.disconnect(websocket)

        print ( "el websocket se desconecto [%i]"%(id_user) )
        await manager.broadcast(f"El usuario  #{id_user} se desconecto")

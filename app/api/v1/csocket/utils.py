
from typing import Optional , List
from fastapi import ( WebSocket , Cookie,Query,status)
from jose import JWTError, jwt
from core.config import SECRET_KEY ,ALGORITHM

from core.extensions import get_session , SessionManager
from core.dbsetup import Usuario , UsuarioPermiso , Grupo , UsuarioGrupo ,Permiso,GrupoPermiso
from fastapi import Request
import json
class ConnectionManager:
    def __init__(self):
        self.active_connections: List[WebSocket] = []
        self.id_usuarios:List[int]=[]
    async def connect(self, websocket: WebSocket=None , id_usuario:int = None):
        if  id_usuario not in self.id_usuarios:
            await websocket.accept()
            self.active_connections.append(websocket)
            self.id_usuarios.append(id_usuario)

            print ( self.id_usuarios , "<<< ids_usuarios en la clase")
            return True
        else : 
            await websocket.close()
            print ("Error el usuario ya existe dentro del pool de usuario")
        return False
    def disconnect(self, websocket: WebSocket):
        indice_socket = self.active_connections.index(websocket)

        self.active_connections.remove(websocket)
        del self.id_usuarios[indice_socket]

    async def send_personal_message(self, message: str, websocket: WebSocket):
        await websocket.send_text(message)
    async def send_message_between(self , data:dict ,ws_origin:WebSocket):
        
        data["from"] =self.id_by_ws(ws_origin)
        data_json = json.dumps(data)
        await self.ws_by_id(id_device=data["to"]).send_json(data_json)

    async def broadcast(self, message: str):
        for connection in self.active_connections:
            await connection.send_text(message)
    def id_by_ws(self, websocket:WebSocket)->int:
        return self.id_usuarios[ self.active_connections.index(websocket) ]
    def ws_by_id(self, id_device:int)->WebSocket:
        return self.active_connections[self.id_usuarios.index(id_device)]

def get_device_db(id_user :int ):

    try:
        session=get_session()
        usuario_actual=session.query( 
            Usuario 
            ).filter( 
                Usuario.id == id_user ,
                Usuario.visible == True
            ).first()
        
        if usuario_actual:
            return usuario_actual
        else : 
            print ( "retorno falso")
            return False
    except Exception as ex:
        import traceback as tb
        tb.print_exc()
        return False
    finally:
        session.remove()

async def validate_cookie(
    websocket: WebSocket,
    cookie_jwt: str = Cookie(None),
    ):
    # print ( "entre en validacion de cookie")
    try : 

        if cookie_jwt is None:
            await websocket.close(code=status.WS_1008_POLICY_VIOLATION)
            # print ( "no hay cookie")
            return False
        
        payload = jwt.decode(token=cookie_jwt,
                             key=SECRET_KEY,
                             algorithms=[ALGORITHM])

        # print (payload)
        id_user = payload.get("id_user")
        # print( [id_user] ,"<<< id usuario obtenido")
        user = get_device_db(id_user =id_user )

        if user :
            return id_user
        else:
            
            await websocket.close(code=status.WS_1008_POLICY_VIOLATION)
            return False
    except:
        import traceback as tb
        tb.print_exc()
        await websocket.close(code=status.WS_1008_POLICY_VIOLATION)
manager =ConnectionManager()

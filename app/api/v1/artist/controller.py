
from fastapi import (APIRouter, Body, Cookie, Depends, FastAPI, File, Header,
                     HTTPException, Query, status, Response)
from app.api.v1.artist.schemas import *
from app.api.v1.artist.use_cases import ArtistUC  ,NewArtistUC , ArtistDetailUc
#, LyricsUC,NewLyricsUC

tags=["ARTISTS"] 
router = APIRouter()

@router.get("", response_model=PagerArtist, tags=tags)
async def get_artist_list( 
    elemento:ArtistUC = Depends(ArtistUC)
):

    return elemento.album_paginated()

@router.post("" , response_model=NewArtistOut , tags=tags)
def create_artist(
    use_case:NewArtistUC =Depends(NewArtistUC)
):
    return use_case.create()

@router.get("/{id_artist}" , response_model=RowArtist , tags=tags)
def retrieve_detail_artist(
    use_case:ArtistDetailUc =Depends(ArtistDetailUc)
):
    retrieved =use_case.retrieve()
    return retrieved

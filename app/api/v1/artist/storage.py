from core.dbsetup import Lyrics , Artists,Albums
from app.api.v1.artist.schemas import PagerArtist , RowArtist


from app.utils.utils import Pager
from typing import List
from core.base_hexagonal import BaseUseCase,BaseStorage
from core.dbsetup import Albums,Songs


from sqlalchemy.exc import IntegrityError
from fastapi.exceptions import HTTPException

class ArtistStorage(BaseStorage):
    

    def paginated(
        self,
        item_pagina:int ,
        pag_actual:int ,
        tipo_filtro:str ="",
        param_filtro:str ="",
    )->PagerArtist:

        db_sentence:List[Lyrics] = self.db.query(
            Artists
        ).with_entities(
           Artists.id,
           Artists.name,
           Artists.start_year
            
            
        )

        current_pager = PagerArtist(
            item_pagina= item_pagina,
            pag_actual=pag_actual,   
        )
        current_pager.paginate(db_sentence=db_sentence)

        return current_pager

    def create(
        self,
        name:str ,
        start_year:int

    )->Artists:
        new=Artists(
            name = name,
            start_year=start_year
        )

        try:
            self.db.add(new)

            self.db.commit()
            return new
        except IntegrityError as ex:
            raise HTTPException(
                status_code=400,
                detail=ex.args
            ) 

    # def detail_lyric(
    #     self,
    #     id_lyric: int,
    # ) -> Lyrics:

    #     return self.db.query(
    #         Lyrics
        
    #     ).filter(
    #         Lyrics.id == id_lyric
    #     ).first()


    def detail(
        self,
        id_artist: int,
    )->RowArtist:
        # current_album = self.detail_lyric(id_lyric=id_lyric)

        data=self.db.query(
            Artists
        
        ).filter(
            Artists.id == id_artist
        ).with_entities(
            Artists.id,
            Artists.name,
            Artists.start_year
        ).first()

        return data

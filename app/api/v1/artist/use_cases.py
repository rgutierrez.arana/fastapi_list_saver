

from app.api.v1.artist.schemas import PagerArtist , NewArtist
from typing import Tuple
from app.api.v1.lyrics.schemas import PagerLyrics 
from app.api.v1.auth.controller import get_current_active_user
from app.utils.utils import Search
from fastapi.params import Depends
from app.utils.general_schemas import User
from core.base_hexagonal import BaseUseCase
from app.api.v1.artist.storage import ArtistStorage


class ArtistUC(BaseUseCase):
    storage :ArtistStorage = None
    def __init__(self,
                 args: Search = Depends(Search),
                 context_user=Depends(get_current_active_user),
                 ) -> None:
        user, db_session = context_user
        self.args:Search = args

        
        super().__init__(
            db=db_session,
            current_user=user,
            storage=ArtistStorage
        )

    def album_paginated(self)->PagerArtist:

        return self.storage.paginated(
            item_pagina= self.args.item_pagina,
            pag_actual=self.args.pag_actual
        )

class NewArtistUC(BaseUseCase):
    storage :ArtistStorage = None
    def __init__(self,
                 args: NewArtist ,
                 context_user=Depends(get_current_active_user),
                 ) -> None:
        user, db_session = context_user

        self.args:NewArtist = args

        
        super().__init__(
            db=db_session,
            current_user=user,
            storage=ArtistStorage
        )

    def create(self):
        return self.storage.create(**self.args.dict())



class ArtistDetailUc(BaseUseCase):
    storage :ArtistStorage = None
    def __init__(self,
                 id_artist:int,
                 context_user=Depends(get_current_active_user),
                 ) -> None:
        user, db_session = context_user

        self.id_artist:int = id_artist

        
        super().__init__(
            db=db_session,
            current_user=user,
            storage=ArtistStorage
        )

    def retrieve(self):
        current_resp =self.storage.detail(id_artist=self.id_artist)
        
        return current_resp

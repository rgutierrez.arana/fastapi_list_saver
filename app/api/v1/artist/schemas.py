
from typing import List, Optional
from app.utils.utils import Search,Pager,OrmModel
from dataclasses import dataclass


class RowArtist(OrmModel):
    id: int = None
    name: Optional[str] = None
    start_year:int 
   

    
class PagerArtist(Pager):

    data:List[RowArtist] = []


class NewArtist(OrmModel):
    name:str 
    start_year:int
    


class NewArtistOut(NewArtist):
    id:int =None
    



from app.data.song.base_songs import Songs
from typing import Tuple
from app.api.v1.songs.schemas import PagerSong , NewSong
from app.api.v1.auth.controller import get_current_active_user
from app.utils.utils import Search
from fastapi.params import Depends
from app.utils.general_schemas import User
from core.base_hexagonal import BaseUseCase
from app.api.v1.songs.storage import SongStorages


class SongListUc(BaseUseCase):
    storage :SongStorages = None

    def __init__(self,
                 args: Search = Depends(Search),
                 context_user=Depends(get_current_active_user),
                 ) -> None:
        user, db_session = context_user
        self.args:Search = args

        
        super().__init__(
            db=db_session,
            current_user=user,
            storage=SongStorages
        )

    def song_paginated(self)->PagerSong:

        return self.storage.paginated_songs(
            item_pagina= self.args.item_pagina,
            pag_actual=self.args.pag_actual
        )

class SongInsertUc(BaseUseCase):
    storage :SongStorages = None

    def __init__(self,
                 args: NewSong ,
                 context_user=Depends(get_current_active_user),
                 ) -> None:
        user, db_session = context_user
        self.args:NewSong = args

        
        super().__init__(
            db=db_session,
            current_user=user,
            storage=SongStorages
        )

    def insert_new_song(self)->Songs:

        return self.storage.add_new_song(name=self.args.name, album_id=self.args.album_id)

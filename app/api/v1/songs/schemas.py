
from typing import List, Optional
from app.utils.utils import Search,Pager,OrmModel
from dataclasses import dataclass


class RowSong(OrmModel):
    id:int
    name:str 
    album_name:str = None
    album_id:int = None

class PagerSong(Pager):
    data:List[RowSong] = []
    
class NewSong(OrmModel):
    name:str 
    album_id:int


class NewSongResp(NewSong):
    id:int
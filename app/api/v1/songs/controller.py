
from app.api.v1.songs.use_cases import SongListUc ,SongInsertUc
from app.api.v1.songs.schemas import PagerSong,NewSongResp
from fastapi import (APIRouter, Body, Cookie, Depends, FastAPI, File, Header,
                     HTTPException, Query, responses, status, Response)


tags=["SONGS"] 
router = APIRouter()



@router.get("", response_model=PagerSong, tags=tags)
async def get_songs( 
    
    elemento:SongListUc = Depends(SongListUc)

):
    response = elemento.song_paginated()
    return response
    


@router.post("", response_model=NewSongResp, tags=tags)
async def insert_song( 
    
    elemento:SongInsertUc = Depends(SongInsertUc)

):
    response = elemento.insert_new_song()
    return response
    
from app.data.album.base_album import Albums
from typing import List
from core.base_hexagonal import BaseStorage
from core.dbsetup import Songs
from app.api.v1.songs.schemas import PagerSong

from sqlalchemy.exc import IntegrityError
from fastapi.exceptions import HTTPException



class SongStorages(BaseStorage):
    

    def paginated_songs(
        self,
        item_pagina:int ,
        pag_actual:int ,
        tipo_filtro:str ="",
        param_filtro:str ="",
    )->PagerSong:

        db_sentence:List[Songs] = self.db.query(
            Songs
        ).join(
            Albums,
            Albums.id == Songs.album_id
        ).with_entities(
            Songs.id,
            Songs.name,
            Albums.name.label("album_name"),
            Albums.id.label("album_id")

        )
        

        current_pager = PagerSong(
            item_pagina= item_pagina,
            pag_actual=pag_actual,
            
        )

        current_pager.paginate(db_sentence=db_sentence)


        return current_pager
    def add_new_song(
        self,
        name:str,
        album_id:int
    )->Songs:
        new_song = Songs(
            name=name,
            album_id=album_id
        )

        self.db.add(new_song)
        self.db.commit()

        return new_song
    # def create_album(
    #     self,
    #     name:str 
    # )->Albums:
    #     new_album=Albums(
    #         name = name
    #     )

    #     try:
    #         self.db.add(new_album)

    #         self.db.commit()
    #         return new_album
    #     except IntegrityError as ex:
    #         raise HTTPException(
    #             status_code=400,
    #             detail=ex.args
    #         ) 

    # def detail_album(
    #     self,
    #     id_album: int,
    # ) -> Albums:

    #     return self.db.query(
    #         Albums
    #     ).filter(
    #         Albums.id == id_album
    #     ).first()

import pydantic.json
from app.utils.general_schemas import * 

class InTipoSitio(OrmModel):

    tipo_sitio:str
    descripcion:str

class FilaTipoSitio(OrmModel):
    id :int
    tipo_sitio :str
    descripcion:str

class OutTipoSitio(OrmModel):
    sitios:List[FilaTipoSitio] = []    
from fastapi import FastAPI
#from starlette_prometheus import metrics, PrometheusMiddleware
# from app.controllers.autos.controller import  router as autos
from app.api.v1.auth.controller import router as auth
from app.api.v1.csocket.controller import router as wsocket_router
from app.api.v1.albums.controller import router as album_router
from app.api.v1.songs.controller import router as song_router
from app.api.v1.lyrics.controller import router as lyric_router
from app.api.v1.artist.controller import router as artist_router
#from app.api.v1.test import router as test
# from app.api.v1.crud.controller import router as crud
# from app.api.v1.selectores.controller import router as selector
# from app.api.v1.general.controller import router as general
# from app.api.v1.administracion_usuarios.controller import router as admin_usuarios
# from app.api.v1.form_solicitud_iniciativa.controller import router as form_solicitud_iniciativa
# from app.api.v1.flujos_inversion.controller import router as flujo_inversion
# from app.api.v1.tags_solicitud.controller import router as tag


from starlette.middleware.cors import CORSMiddleware
from sentry_sdk import init as initialize_sentry
from sentry_sdk.integrations.sqlalchemy import SqlalchemyIntegration
from core.factories import settings
from core.dbsetup import Base
from starlette.requests import Request

from fastapi.openapi.docs import (
    get_redoc_html,
    get_swagger_ui_html,
    get_swagger_ui_oauth2_redirect_html,
)


from app.generic_documentation.documentation import generar_documentacion

from core.extensions import session

#app = FastAPI()
app = FastAPI(docs_url=None, 
    title="API V1.0",
    redoc_url=None,
    openapi_url="/swagger/openapi.json",
    swagger_ui_oauth2_redirect_url="/swagger/docs/oauth2-redirect",
)

@app.on_event("startup")
async def startup():
    
    print("app started")


@app.on_event("shutdown")
async def shutdown():
    print("SHUTDOWN")


cors_origins = [i.strip() for i in settings.CORS_ORIGINS.split(",")]
app.add_middleware(
        CORSMiddleware,
        allow_origins="*",
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )


# @app.middleware("http")
# async def close_session_conection(request: Request, call_next):

#     response = await call_next(request)
#     session.remove()
    
#     return response
#app.add_middleware(PrometheusMiddleware)
#app.add_route("/metrics/",metrics)

# app.include_router(router= autos , prefix="/api/v1.0/test")

#subapi = FastAPI()
#subapi.include_router(router=general , prefix="/v1.0/general")
#app.mount("/subapi",subapi,name="subapi")
generar_documentacion(app=app)


# authAPI = FastAPI()
# selectorAPI = FastAPI()
# crudAPI = FastAPI()
# admin_usuariosAPI = FastAPI()
# form_solicitud_iniciativaAPI = FastAPI()
# flujo_inversionAPI = FastAPI()




# flujo_inversionAPI.include_router(router=form_solicitud_iniciativa , prefix="")
# selectorAPI.include_router(router=selector , prefix="")
# crudAPI.include_router(router=crud , prefix="")
# authAPI.include_router(router=auth , prefix="")
# admin_usuariosAPI.include_router(router=admin_usuarios , prefix="")
# form_solicitud_iniciativaAPI.include_router(router=form_solicitud_iniciativa , prefix="")


#app.mount("/api/v1.0/auth" , authAPI , name="authAPI")
#app.mount("/api/v1.0/solicitud-inicativa" , form_solicitud_iniciativaAPI , name="form_solicitud_iniciativaAPI")
#app.mount("/api/v1.0/crud" , crudAPI , name="crudAPI")
#app.mount("/api/v1.0/administracion" , admin_usuariosAPI , name="admin_usuariosAPI")
#app.mount("/api/v1.0/flujo-inversion" , flujo_inversionAPI , name="flujo_inversionAPI")
#app.mount("/api/v1.0/selector" , selectorAPI , name="selectorAPI")


# app.include_router(router=general , prefix="/api/v1.0/general")
app.include_router(router=auth , prefix="/api/v1.0/auth")
app.include_router(router=album_router, prefix="/api/v1.0/albums")
app.include_router(router=song_router, prefix="/api/v1.0/songs")
app.include_router(router=lyric_router, prefix="/api/v1.0/lyric")
app.include_router(router=artist_router, prefix="/api/v1.0/artist")


# app.include_router(router=wsocket_router  , prefix="/api/v1.0/ws")
# app.include_router(router=crud , prefix="/api/v1.0/crud")
# app.include_router(router=selector , prefix="/api/v1.0/selector")
# app.include_router(router=admin_usuarios , prefix="/api/v1.0/administracion")
# app.include_router(router=form_solicitud_iniciativa , prefix="/api/v1.0/solicitud-inicativa")
# app.include_router(router=flujo_inversion , prefix="/api/v1.0/flujo-inversion")
# app.include_router(router=tag , prefix="/api/v1.0/tags")



#app.include_router(router=test , prefix="/api/v1.0/test")





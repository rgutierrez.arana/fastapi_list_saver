# write you database models in this file
from app.utils.base_model import BaseClass
from core.dbsetup import (
    Column,
    BigInteger,
    UUIDType,
    relationship,
    String,
    Integer,
    ForeignKey,
    Boolean,
    Float,
    TIMESTAMP,
    Enum,
    ForeignKey,
    Sequence
)

from datetime import datetime
from core.extensions import base




class Colaborador(base, BaseClass):
    __tablename__ = "tm_colaborador"
    __table_args__ = {
        'comment': 'Tabla de datos de colaborador',
        # 'autoload': True,
        # "extend_existing":True
    }
    #field_seq = Sequence(__tablename__ + "_seq")
    # id = Column(BigInteger  ,primary_key=True , autoincrement=True)
    nombre = Column(String(96), nullable=False, unique=False)
    apellido = Column(String(96), nullable=False, unique=False)

    #fecha_registro = Column(TIMESTAMP, nullable=False, default=datetime.now, comment="fecha de creacion del usuario")
    #fecha_modificacion = Column(TIMESTAMP, nullable=False, default=datetime.now, onupdate=datetime.now ,comment="fecha de actualizacion del usuario")
    visible = Column(Boolean, server_default="1", nullable=False, comment="define si el colaborador es visible para los listados")
    activo = Column(Boolean , server_default="1", nullable=False )

from app.utils.base_model import *

class Biblioteca(base,BaseClass):
    __tablename__ = "tm_biblioteca"
    
    id = Column(BigInteger, primary_key=True)
    nombre = Column(String(length=128), nullable=True)
    ruta = Column(String(length=512), nullable=True)
    id_usuario = Column(Integer,ForeignKey("tm_usuario.id"), nullable=False)
    id_tipo_archivo = Column(Integer,ForeignKey("ts_tipo_archivo.id"), nullable=False)
    id_auxiliar = Column(Integer, nullable=False)

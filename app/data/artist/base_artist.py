# write you database models in this file

from sqlalchemy.sql.expression import null
from sqlalchemy.sql.sqltypes import Text
from app.utils.base_model import BaseClass
from core.dbsetup import (
    Column,
    BigInteger,
    UUIDType,
    relationship,
    String,
    Integer,
    ForeignKey,
    Boolean,
    Float,
    TIMESTAMP,
    Enum,
    ForeignKey,
    Sequence
)

from datetime import datetime
from core.extensions import base

# import enum

class Artists(base,BaseClass):
    __tablename__ = "tm_artists"    
    name = Column(String(length=256), nullable=True, index=True, unique=True)
    start_year = Column(Integer, nullable=True)
    
    
    

    

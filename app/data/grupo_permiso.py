# write you database models in this file
from app.utils.base_model import BaseClass
from core.dbsetup import (
    Column,
    BigInteger,
    UUIDType,
    relationship,
    String,
    Integer,
    ForeignKey,
    Boolean,
    Float,
    TIMESTAMP,
    Enum,
    ForeignKey,
    Sequence
)

from datetime import datetime
from core.extensions import base

import enum


class GrupoPermiso(base,BaseClass):
    __tablename__ = "tm_grupo_permiso"
    __table_args__ = {'comment': 'Tabla de datos de grupo permiso',
                      # 'autoload': True,
                      # "extend_existing":True
                      }
    
    # id = Column(BigInteger,primary_key=True,autoincrement=True)
    #nombre = Column(String(96), nullable=False, unique=False)
    
    #fecha_registro = Column(TIMESTAMP, nullable=False, default=datetime.now, comment="fecha de creacion de la viceprecidencia")
    #fecha_modificacion = Column(TIMESTAMP, nullable=False, default=datetime.now, onupdate=datetime.now, comment="fecha de actualizacion del la viceprecidencia")
    visible = Column(Boolean, server_default="1", nullable=False, comment="define si el viceprecidencia es visible para los listados")
    activo = Column(Boolean, server_default="1", nullable=False)

    id_permiso = Column(Integer , ForeignKey("tm_permiso.id") , nullable=True ,comment = "Id permiso relacionado ")
    id_grupo = Column(Integer , ForeignKey("tm_grupo.id") , nullable=True ,comment = "Id grupo relacionado")
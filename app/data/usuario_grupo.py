# write you database models in this file
from app.utils.base_model import BaseClass
from core.dbsetup import (
    Column,
    BigInteger,
    UUIDType,
    relationship,
    String,
    Integer,
    ForeignKey,
    Boolean,
    Float,
    TIMESTAMP,
    Enum,
    ForeignKey,
    Sequence
)

from datetime import datetime
from core.extensions import base

import enum


class UsuarioGrupo(base,BaseClass):
    __tablename__ = "tm_usuario_grupo"
    __table_args__ = {
        'comment': 'Tabla de datos de usuario grupo',
        # 'autoload': True
        # "extend_existing":True

    }

    # id = Column(BigInteger, primary_key=True,autoincrement=True)

    # fecha_registro = Column(TIMESTAMP, nullable=False, default=datetime.now, comment="fecha de creacion de la jefatura")
    # fecha_modificacion = Column(TIMESTAMP, nullable=False, default=datetime.now, onupdate=datetime.now,
    #                             comment="fecha de actualizacion del la jefatura")
    visible = Column(Boolean, server_default="1", nullable=False,
                     comment="define si el jefatura es visible para los listados")
    activo = Column(Boolean, server_default="1", nullable=False)

    id_usuario = Column(Integer, ForeignKey("tm_usuario.id"), nullable=True, comment="Id de la del usuario relacionado")
    usuario = relationship("Usuario", lazy="select", backref="usuario_grupo")

    id_usuario = Column(Integer , ForeignKey("tm_usuario.id") , nullable=True ,comment = "Id de la del usuario relacionado")


    id_grupo =  Column(Integer , ForeignKey("tm_grupo.id") , nullable=True ,comment = "Id del grupo")
    

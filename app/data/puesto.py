# write you database models in this file
from core.dbsetup import (
    Column,
    BigInteger,
    UUIDType,
    relationship,
    String,
    Integer,
    ForeignKey,
    Boolean,
    Float,
    TIMESTAMP,
    Enum,
    ForeignKey,
    Sequence
)

from datetime import datetime
from core.extensions import base



class PuestoColaborador(base):
    __tablename__ = "tm_puesto_colaborador"
    __table_args__ = {'comment': 'Tabla de datos de puesto',
        # 'autoload': True,
        # "extend_existing":True
        }



    id = Column(BigInteger, primary_key=True)
    nombre = Column(String(96), nullable=False, unique=False)
    fecha_registro = Column(TIMESTAMP, nullable=False, default=datetime.now,
                            comment="fecha de creacion de la viceprecidencia")
    fecha_modificacion = Column(TIMESTAMP, nullable=False, default=datetime.now,
                                onupdate=datetime.now, comment="fecha de actualizacion del la viceprecidencia")
    visible = Column(Boolean, server_default="1", nullable=False,
                     comment="define si el viceprecidencia es visible para los listados")

    activo = Column(Boolean, server_default="1", nullable=False)

    palabras_clave = Column(String(512) , nullable = True , comment = "Palabras clave para asociar al LDAP" )

    # id_jefatura = Column(Integer , ForeignKey("tm_jefatura.id") , nullable=True ,comment = "Id de la jefatura asociada al puesto")
    # jefatura = relationship("Jefatura" , lazy="select" ,backref = "puesto")

# write you database models in this file
from app.utils.base_model import BaseClass
from core.dbsetup import (
    Column,
    BigInteger,
    UUIDType,
    relationship,
    String,
    Integer,
    ForeignKey,
    Boolean,
    Float,
    TIMESTAMP,
    Enum,
    ForeignKey,
    Sequence
)
from datetime import datetime
from core.extensions import base

class Grupo(base,BaseClass):
    __tablename__ = "tm_grupo"
    __table_args__ = {
        'comment': 'Tabla de datos de grupo',
        #'autoload': True
        #"extend_existing":True

    }
    #field_seq = Sequence(__tablename__ + "_seq")
    # id = Column(BigInteger, primary_key=True , autoincrement=True)
    nombre =  Column(String(96), nullable=False, unique=True)
    #fecha_registro = Column(TIMESTAMP, nullable=False, default=datetime.now, comment="fecha de creacion del grupo")
    #fecha_modificacion = Column(TIMESTAMP, nullable=False, default=datetime.now, onupdate=datetime.now ,comment="fecha de actualizacion del grupo")
    visible = Column(Boolean, server_default="1", nullable=False, comment="define si el grupo es visible para los listados")
    activo = Column(Boolean , server_default="1", nullable=False )

   
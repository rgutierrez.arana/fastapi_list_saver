from app.utils.base_model import *

class TipoArchivo(base,BaseClass):
    __tablename__ = "ts_tipo_archivo"

    id = Column(BigInteger, primary_key=True)
    nombre = Column(String(length=128), nullable=True)


# write you database models in this file
from sqlalchemy.orm import backref
from app.utils.base_model import BaseClass
from core.dbsetup import (
    Column,
    BigInteger,
    UUIDType,
    relationship,
    String,
    Integer,
    ForeignKey,
    Boolean,
    Float,
    TIMESTAMP,
    Enum,
    ForeignKey,
    Sequence
)

from datetime import datetime
from core.extensions import base

# import enum

class Albums(base,BaseClass):
    __tablename__ = "tm_albums"    
    name = Column(String(length=128), nullable=True, index=True, unique=True)


    
    id_artista = Column(Integer , ForeignKey("tm_artists.id") , nullable=True )
    

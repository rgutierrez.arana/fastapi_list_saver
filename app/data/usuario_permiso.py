


# write you database models in this file
from app.utils.base_model import BaseClass
from core.dbsetup import (
    Column,
    BigInteger,
    UUIDType,
    relationship,
    String,
    Integer,
    ForeignKey,
    Boolean,
    Float,
    TIMESTAMP,
    Enum,
    ForeignKey,
    Sequence
)

from datetime import datetime
from core.extensions import base

import enum


class UsuarioPermiso(base,BaseClass):
    __tablename__ = "tm_usuario_permiso"
    __table_args__ = {'comment': 'Tabla de datos de  usuario_permiso',
                      # 'autoload': True,
                      # "extend_existing":True
                      }

    #id = Column(BigInteger, primary_key=True,autoincrement=True)
    
    
    #fecha_registro = Column(TIMESTAMP, nullable=False, default=datetime.now, comment="fecha de creacion del usuario_permiso")
    #fecha_modificacion = Column(TIMESTAMP, nullable=False, default=datetime.now, onupdate=datetime.now, comment="fecha de actualizacion del usuario_permiso")
    visible = Column(Boolean, server_default="1", nullable=False, comment="define si el usuario_permiso es visible para los listados")
    activo = Column(Boolean, server_default="1", nullable=False)

    id_usuario =Column(Integer , ForeignKey("tm_usuario.id") , nullable=True ,comment = "Id de  del usuario relacionado")
    id_permiso =Column(Integer , ForeignKey("tm_permiso.id") , nullable=True ,comment = "Id del permiso relacionado")
    
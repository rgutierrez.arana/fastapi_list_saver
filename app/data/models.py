# write you database models in this file
from core.dbsetup import (
    Column,
    BigInteger,
    UUIDType,
    relationship,
    String,
    Integer,
    ForeignKey,
    Boolean,
    Float,
    TIMESTAMP,
    Sequence
)

from datetime import datetime
from core.extensions import base



class Autos(base):
    __tablename__ = "tm_autos"
    __table_args__ = {
        'comment': 'En esta tabla de autos'
    }
    field_seq = Sequence(__tablename__ + "_seq")
    id      = Column(BigInteger, primary_key=True,server_default = field_seq.next_value() )
    placa   = Column(String(64) , nullable=False )
    visible = Column(Boolean, server_default="1", nullable=False, comment="")
    created = Column(TIMESTAMP, nullable=False, default=datetime.now, comment="")
    updated = Column(TIMESTAMP, nullable=False, default=datetime.now, onupdate=datetime.now ,comment="")



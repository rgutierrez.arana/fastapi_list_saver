# write you database models in this file

from sqlalchemy.sql.expression import null
from sqlalchemy.sql.sqltypes import Text
from app.utils.base_model import BaseClass
from core.dbsetup import (
    Column,
    BigInteger,
    UUIDType,
    relationship,
    String,
    Integer,
    ForeignKey,
    Boolean,
    Float,
    TIMESTAMP,
    Enum,
    ForeignKey,
    Sequence
)

from datetime import datetime
from core.extensions import base

# import enum

class Lyrics(base,BaseClass):
    __tablename__ = "tm_lyrics"    
    name = Column(String(length=128), nullable=True, index=True, unique=True)
    text = Column(Text , nullable=True)
    song_id = Column(Integer, ForeignKey("tm_songs.id", ondelete="CASCADE"), index=True, nullable=False)
    

    

# write you database models in this file
from app.utils.base_model import BaseClass
from core.dbsetup import (
    Column,
    BigInteger,
    UUIDType,
    relationship,
    String,
    Integer,
    ForeignKey,
    Boolean,
    Float,
    TIMESTAMP,
    Enum,
    ForeignKey,
    Sequence
)

from datetime import datetime
from core.extensions import base

import enum


class Permiso(base,BaseClass):
    __tablename__ = "tm_permiso"
    __table_args__ = {'comment': 'Tabla de datos de  permiso',
                      # 'autoload': True,
                      # "extend_existing":True
                      }

    # id = Column(BigInteger, primary_key=True, autoincrement=True)
    
    codigo = Column(String(128) , nullable=False )
    #fecha_registro = Column(TIMESTAMP, nullable=False, default=datetime.now, comment="fecha de creacion del permiso")
    #fecha_modificacion = Column(TIMESTAMP, nullable=False, default=datetime.now, onupdate=datetime.now, comment="fecha de actualizacion del permiso")
    visible = Column(Boolean, server_default="1", nullable=False, comment="define si el permiso es visible para los listados")
    activo = Column(Boolean, server_default="1", nullable=False)


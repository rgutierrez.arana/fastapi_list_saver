# write you database models in this file
from app.utils.base_model import BaseClass
from core.dbsetup import (
    Column,
    BigInteger,
    UUIDType,
    relationship,
    String,
    Integer,
    ForeignKey,
    Boolean,
    Float,
    TIMESTAMP,
    Enum,
    Sequence
)
from uuid import uuid4
from datetime import datetime
from core.extensions import base
from core.config import SECRET_KEY
from bcrypt import hashpw, gensalt, checkpw
import enum


class Acceso(enum.Enum):
    web = 1
    app = 2


class Usuario(base,BaseClass):
    __tablename__ = "tm_usuario"
    __table_args__ = {
        'comment': 'En esta tabla de usuarios registrados',
        # 'autoload': True,
        # "extend_existing":True
    }
   
    
    username = Column(String(256), nullable=False, unique=True)
    correo = Column(String(256), nullable=False)
    hash_password = Column(String(512), nullable=True)
    estado = Column(String(2), nullable=True)
    nro_intentos = Column(Integer, nullable=True, server_default="0", default=0)
    tipo_acceso = Column(Enum(Acceso), nullable=True, comment="tipo de acceso del usuario")
    # fecha_registro = Column(TIMESTAMP, nullable=False, default=datetime.now, comment="fecha de creacion del usuario")
    # fecha_modificacion = Column(TIMESTAMP, nullable=False, default=datetime.now, onupdate=datetime.now,
    #                             comment="fecha de actualizacion del usuario")
    fecha_acceso = Column(TIMESTAMP, nullable=False, default=datetime.now, onupdate=datetime.now,
                          comment="fecha de ultimo acceso del usuario")
    visible = Column(Boolean, server_default="1", nullable=False,
                     comment="define si el usuario es visible para los listados")
    id_colaborador = Column(Integer, ForeignKey("tm_colaborador.id"), nullable=True,
                            comment="Id del colaborador para la relacion")
    colaborador = relationship("Colaborador", lazy="select", backref="usuario")
    
    apikey= Column(String(256), nullable=True)

    @property
    def password(self):
        raise AttributeError('password: solo funcion de escritura')

    @password.setter
    def password(self, password: str):
        # encrypt_password = flask.current_app.config['ENCRYPT_PASSWORD']
        salt = gensalt()
        u8_password = password.encode("utf-8")
        hashed = hashpw(password=u8_password, salt=salt).decode("utf-8")
        self.hash_password = hashed

        ## tamanio hashed password[60] + hash[29] = 89
    @classmethod
    def check_password(cls, password:str="" , hashed_passwd:str=None):
        if hashed_passwd ==None:
            return checkpw(password=password.encode()  ,hashed_password= cls.hash_password.encode()) 
        else :
            return checkpw(password=password.encode()  ,hashed_password= hashed_passwd.encode())     
        

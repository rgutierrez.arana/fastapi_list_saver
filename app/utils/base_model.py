from core.dbsetup import (
    Column,
    BigInteger,
    UUIDType,
    relationship,
    String,
    Integer,
    ForeignKey,
    Boolean,
    Float,
    TIMESTAMP,
    Enum,
    ForeignKey,
    Sequence
)

from datetime import datetime
from core.extensions import base
from typing import Any


class BaseClass:
    id = Column(Integer ,autoincrement=True , primary_key=True)
    visible = Column(Boolean, server_default="1", nullable=False, comment="define si  es visible")
    activo = Column(Boolean , server_default="1", nullable=False )
    fecha_registro = Column(TIMESTAMP, nullable=False, default=datetime.now, comment="fecha de creacion " )
    fecha_modificacion = Column(TIMESTAMP, nullable=False, default=datetime.now, onupdate=datetime.now ,comment="fecha de actualizacion " )

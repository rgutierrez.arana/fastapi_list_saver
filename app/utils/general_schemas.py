from pydantic import BaseModel,constr,validator,ValidationError,EmailStr 
from typing import Optional , List , Dict ,Any
from datetime import datetime,date
from dataclasses import dataclass
from fastapi import Query
class OrmModel(BaseModel):
    class Config:
        orm_mode = True

class StatusSuccess(OrmModel):
    status:str="success"
class User(OrmModel):
    id : int
    username: str
    correo: Optional[str] = None
success = {"status" :  "success"}

class DefaultFile(OrmModel):
    ruta_archivo:str
from pydantic import BaseModel,constr,validator,ValidationError,EmailStr 
from typing import Optional , List , Dict ,Any
from app.utils.general_schemas import OrmModel
from dataclasses import dataclass
from fastapi import  Depends,Header,File, Body,Query
import math
from datetime import datetime
class ClassNull(OrmModel):
    value:int = None
class Pager(OrmModel):
    pag_anterior:int=None
    pag_actual:int=None
    pag_siguiente:int=None
    pag_total:int=None
    item_pagina:int=None
    item_desde:int=None
    item_hasta:int=None
    item_total:int=None
    data:List[ClassNull]=[]
    
    def paginate(self, db_sentence=None):
        self.item_pagina=self.item_pagina
        offset = (self.pag_actual-1)*self.item_pagina
        self.item_total= db_sentence.count()
        self.data=[]
        if self.item_total == 0:
            self.data = []
        else:
            self.data=db_sentence.offset(offset).limit( self.item_pagina ).all()

        #data_class:ClassNull=self.__annotations__["data"].__args__[0]

        self.pag_anterior = self.pag_actual - 1
        self.item_desde = 0 if self.item_total == 0 else (self.pag_anterior * self.item_pagina) + 1
        self.item_hasta = 0 if self.item_total == 0 else (self.pag_actual * self.item_pagina)
        self.pag_anterior = None if self.pag_anterior == 0 else self.pag_anterior

        self.pag_total = math.ceil(self.item_total / self.item_pagina)

        self.pag_siguiente = self.pag_actual + 1
        self.pag_siguiente = None if (self.pag_actual == self.pag_total or self.item_total == 0) else self.pag_siguiente
        self.item_hasta = self.item_total if self.item_hasta > self.item_total else self.item_hasta



    # class Config:
    #     orm_mode = True

        

@dataclass
class Search:
    tipo_filtro: str = Query("")
    param_filtro: str = Query("")
    item_pagina: int = Query(10,gt=0)
    pag_actual: int = Query(1,gt=0)

@dataclass
class SearchFiltro:

    
    item_pagina: int = Query(10,gt=0)
    pag_actual: int = Query(1,gt=0)

class FieldSelector(OrmModel):
    id:int
    codigo:Optional[str]
    text:str
class Selector(OrmModel):
    data:List[FieldSelector]


class FilaStandar(OrmModel):
    visible:int = None
    activo:int = None
    fecha_registro:datetime = None
    fecha_modificacion:datetime = None

class FilaStandarActualizacion(OrmModel):
    
    visible:Optional[int]
    activo:Optional[int]


    
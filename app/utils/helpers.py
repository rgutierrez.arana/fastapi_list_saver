import shutil
from pathlib import Path
from fastapi import UploadFile
from core.config import BIBLIOTECA
from app.data.biblioteca_archivos import Biblioteca
import os
import uuid


import base64
import hashlib
import calendar
import datetime

ruta_actual = os.path.join( os.getcwd() ,BIBLIOTECA ) 


def save_upload_file(upload_file: UploadFile, destination: Path) -> None:
    unico="%s_%s"%(str(uuid.uuid1()),upload_file.filename)
    if type(destination) == str:
        carpeta = os.path.join( ruta_actual,destination)
        virtual_dest = os.path.join(BIBLIOTECA,destination,unico)
        destination=Path( os.path.join( carpeta,unico) ) 

    if not os.path.exists(carpeta):
        
        os.makedirs(carpeta)
    
    try:
        with destination.open("wb") as buffer:
            shutil.copyfileobj(upload_file.file, buffer)
            return {"ruta":virtual_dest , "nombre" : unico}
            
        
    finally:
        upload_file.file.close()

def guardar_array_doc (upload_file:list , destination:str):
    for archivo in upload_file:
        yield save_upload_file(upload_file=archivo ,destination=destination)

def guardar_documento_db(db , nombre:str,ruta:str , id_usuario:int ,id_doc:int , id_auxiliar:int):
    data = Biblioteca(
        nombre=nombre,
        ruta=ruta,
        id_usuario=id_usuario,
        id_tipo_archivo=id_doc,
        id_auxiliar=id_auxiliar

    )
    db.add(data)
    db.commit()

def get_secure_hash(ruta_archivo:str="",secure:str="",expira_segundos:int=600):

    secret = secure
    url = ruta_archivo

    future = datetime.datetime.utcnow() + datetime.timedelta(seconds=expira_segundos)
    expiry = calendar.timegm(future.timetuple())

    secure_link = "{key}{expiry}".format(key=secret,
                                          #    url=url,
                                              expiry=expiry)
    hash = hashlib.md5(secure_link.encode("utf-8") ).digest()
    encoded_hash = base64.urlsafe_b64encode(hash).decode("utf-8")
    encoded_hash=encoded_hash.rstrip("=")
    
    #.rstrip('=')
    return {"url": url, "st": encoded_hash, "e": expiry}

from dotenv import load_dotenv
from pathlib import Path
env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)
import os

import ldap3
from ldap3.core.exceptions import LDAPException

# LDAP_AUTH_URL = 'ldap://%s:%d' % ('14.240.4.146', 389)
# LDAP_AUTH_ACTIVE_DIRECTORY_DOMAIN =os.getenv("LDAP_AUTH_ACTIVE_DIRECTORY_DOMAIN")
# LDAP_AUTH_URL =os.getenv("LDAP_AUTH_URL")
# LDAP_AUTH_CONNECT_TIMEOUT =os.getenv("LDAP_AUTH_CONNECT_TIMEOUT")
# LDAP_AUTH_AUTHENTICATION =os.getenv("LDAP_AUTH_AUTHENTICATION")
# LDAP_AUTH_RECEIVE_TIMEOUT =os.getenv("LDAP_AUTH_RECEIVE_TIMEOUT")
# LDAP_AUTH_USE_TLS =os.getenv("LDAP_AUTH_USE_TLS")
# LDAP_AUTH_SEARCH_BASE =os.getenv("LDAP_AUTH_SEARCH_BASE")

# INPUTS


class BaseLdap:
    LDAP_AUTH_ACTIVE_DIRECTORY_DOMAIN =os.getenv("LDAP_AUTH_ACTIVE_DIRECTORY_DOMAIN")
    LDAP_AUTH_URL =os.getenv("LDAP_AUTH_URL")
    LDAP_AUTH_CONNECT_TIMEOUT =os.getenv("LDAP_AUTH_CONNECT_TIMEOUT")
    LDAP_AUTH_AUTHENTICATION =os.getenv("LDAP_AUTH_AUTHENTICATION")
    LDAP_AUTH_RECEIVE_TIMEOUT =os.getenv("LDAP_AUTH_RECEIVE_TIMEOUT")
    LDAP_AUTH_USE_TLS =os.getenv("LDAP_AUTH_USE_TLS")
    LDAP_AUTH_SEARCH_BASE =os.getenv("LDAP_AUTH_SEARCH_BASE")
    
    username: str = None
    password: str = None
    def __init__(self , username:str=None , password:str = None):

        self.username= username
        self.password= password
        self.coneccion = None

    def conectar(self, username: str = None, password: str = None):
        if username != None:
            username = self.username
        if password != None:
            password = self.password

            


        username = "{domain}\\{username}".format(
                    domain=self.LDAP_AUTH_ACTIVE_DIRECTORY_DOMAIN,
                    username=username,
                )

        try:
            self.coneccion:ldap3.Connection= ldap3.Connection(
                                            ldap3.Server(
                                                self.LDAP_AUTH_URL,
                                                allowed_referral_hosts=[("*", True)],
                                                get_info=ldap3.NONE,
                                                connect_timeout=self.LDAP_AUTH_CONNECT_TIMEOUT,
                ),
                user=username,
                password=password,
                authentication=self.LDAP_AUTH_AUTHENTICATION,
                auto_bind=False,
                raise_exceptions=True,
                receive_timeout=self.LDAP_AUTH_RECEIVE_TIMEOUT,
            )
        except LDAPException as ex:
            print("LDAP connect failed: {ex}".format(ex=ex))
            

        try:
            # Start TLS, if requested.
            if self.LDAP_AUTH_USE_TLS:
                self.coneccion.start_tls(read_server_info=False)
            # Perform initial authentication bind.
            self.coneccion.bind(read_server_info=True)

            if self.coneccion.search(
                    search_base=self.LDAP_AUTH_SEARCH_BASE,
                    search_filter=f'(&(sAMAccountName={username}))',
                    search_scope=ldap3.SUBTREE,
                    attributes=ldap3.ALL_ATTRIBUTES,
                    get_operational_attributes=True,
                    size_limit=1,
            ):
                data = self.coneccion.response[0]
                print(data)
                return data
                
            else:
                print("LDAP user lookup failed")
                return None


        except LDAPException as ex:
            print("LDAP bind failed: {ex}".format(ex=ex))
        
        finally:
            self.coneccion.unbind()
        


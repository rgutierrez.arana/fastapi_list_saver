
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from sqlalchemy.orm import column_property
from sqlalchemy import cast, Date, or_, and_, func, select ,join,String,extract
from sqlalchemy.sql.expression import join




from core.dbsetup import Gerencia , Jefatura , DescripcionTipoIniciativa , Usuario ,Colaborador ,EjesEstratejicos ,Requerimientos , DescripcionTipoInversion
from core.dbsetup import Automatizacion,ImpactoNoEjecutarIniciativa,CapaRed , CapaRedMacro , EstadosSolicitud , Biblioteca 
from core.dbsetup  import SolicitudIniciativa , FlujoInversion


SolicitudIniciativa.ruta = column_property(

    select(
        [Biblioteca.ruta]
    ).where(
        Biblioteca.id_auxiliar == SolicitudIniciativa.id

    ).correlate_except(Biblioteca).limit(1)

)


SolicitudIniciativa.nombre_gerencia_sponsor = column_property(
    select(
        [Gerencia.nombre]
    ).where(
        Gerencia.id == SolicitudIniciativa.id_gerencia_sponsor
    ).correlate_except(Gerencia).limit(1)
    
)

SolicitudIniciativa.nombre_jefatura_solicitante = column_property(
    select(
        [Jefatura.nombre]
    ).where(
        Jefatura.id == SolicitudIniciativa.id_jefatura_solicitante
    ).correlate_except(Jefatura).limit(1)

)
SolicitudIniciativa.nombre_tipo_iniciativa = column_property(
    select(
        [DescripcionTipoIniciativa.tipo_iniciativa]
    ).where(
        DescripcionTipoIniciativa.id == SolicitudIniciativa.id_tipo_iniciativa
    ).correlate_except(DescripcionTipoIniciativa).limit(1)

)
j1 = join(Usuario, Colaborador,
        Usuario.id_colaborador == Colaborador.id)

    
SolicitudIniciativa.nombre_usuario_solicitante=column_property ( 
    select(
        [Colaborador.nombre + " " +Colaborador.apellido]
    ).select_from( 
            j1
        ).where(
        Usuario.id == SolicitudIniciativa.id_usuario_solicitante
    ).correlate_except(Usuario,Colaborador).limit(1)   
)

SolicitudIniciativa.nombre_usuario=column_property ( 
    select(
        [Colaborador.nombre + " " +Colaborador.apellido]
    ).select_from( 
            j1
        ).where(
        Usuario.id == SolicitudIniciativa.id_usuario_solicitante
    ).correlate_except(Usuario,Colaborador).limit(1)   
)


SolicitudIniciativa.nombre_eje_estratejico = column_property(
    select(
        [EjesEstratejicos.eje_estratejico]
    ).where(
        EjesEstratejicos.id == SolicitudIniciativa.id_eje_estratejico
    ).correlate_except(EjesEstratejicos).limit(1)
)
SolicitudIniciativa.nombre_requerimiento = column_property(
    select(
        [Requerimientos.requerimiento]
    ).where(
        Requerimientos.id == SolicitudIniciativa.id_requerimiento
    ).correlate_except(Requerimientos).limit(1)
)

SolicitudIniciativa.nombre_tipo_inversion  = column_property(
    select(
        [DescripcionTipoInversion.tipo_iniciativa]
    ).where(
        DescripcionTipoInversion.id == SolicitudIniciativa.id_tipo_inversion
    ).correlate_except(DescripcionTipoInversion).limit(1)
)

SolicitudIniciativa.nombre_automatizacion  = column_property(
    select(
        [Automatizacion.tipo_iniciativa]
    ).where(
        Automatizacion.id == SolicitudIniciativa.id_automatizacion
    ).correlate_except(Automatizacion).limit(1)
)

SolicitudIniciativa.nombre_impacto_no_ejecutar_iniciativa = column_property(
    select(
        [ImpactoNoEjecutarIniciativa.escala]
    ).where(
        ImpactoNoEjecutarIniciativa.id == SolicitudIniciativa.id_impacto_no_ejecutar_iniciativa
    ).correlate_except(ImpactoNoEjecutarIniciativa).limit(1)

)

SolicitudIniciativa.nombre_impacto_no_ejecutar_iniciativa = column_property(
    select(
        [ImpactoNoEjecutarIniciativa.escala]
    ).where(
        ImpactoNoEjecutarIniciativa.id == SolicitudIniciativa.id_impacto_no_ejecutar_iniciativa
    ).correlate_except(ImpactoNoEjecutarIniciativa).limit(1)

)

SolicitudIniciativa.nombre_capa_red_macro= column_property(

    select(
        [CapaRedMacro.nombre]
    ).where(
        CapaRedMacro.id == SolicitudIniciativa.id_capa_red_macro
    ).correlate_except(CapaRedMacro).limit(1)       
)

SolicitudIniciativa.nombre_capa_red= column_property(

    select(
        [CapaRed.subcapa_red]
    ).where(
        CapaRed.id == SolicitudIniciativa.id_capa_red
    ).correlate_except(CapaRed).limit(1)       
)

SolicitudIniciativa.estado_solicitud = column_property(

    select(
        [EstadosSolicitud.nombre]
    ).where(
        EstadosSolicitud.id == SolicitudIniciativa.id_estado_solicitud
    ).correlate_except(EstadosSolicitud).limit(1)       
)


SolicitudIniciativa.estado_solicitud = column_property(

    select(
        [EstadosSolicitud.nombre]
    ).where(
        EstadosSolicitud.id == SolicitudIniciativa.id_estado_solicitud
    ).correlate_except(EstadosSolicitud).limit(1)       
)


SolicitudIniciativa.importes_totales= column_property(

    select(
        [func.sum( FlujoInversion.importe)]
    ).where(
        FlujoInversion.id_solicitud_iniciativa ==SolicitudIniciativa.id
    ).correlate_except(FlujoInversion)
)
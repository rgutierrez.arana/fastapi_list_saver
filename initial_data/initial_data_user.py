
from core.dbsetup import Permiso,UsuarioGrupo,Usuario,Grupo,GrupoPermiso,Colaborador,Albums

def generate_base_permisions(db):
    nuevo_permiso=Permiso(
        codigo = "ADMINITRADOR"
    )
    db.add(nuevo_permiso)

def generate_admin_user(db):

    nuevo_grupo = Grupo(
        nombre="administrador"
    )
    nuevo_colaborador = Colaborador(
        nombre= "admin",
        apellido= "super_admin",
    )
    db.add(nuevo_grupo)
    db.add(nuevo_colaborador)

    db.flush()

    nuevo_usuario = Usuario(
        username="admin",
        correo="admin@example.com",
        password="admin",
        tipo_acceso="web",
        id_colaborador = nuevo_colaborador.id,
    )

    db.add(nuevo_usuario)

    nuevo_usuario_grupo = UsuarioGrupo(
        id_usuario = nuevo_usuario.id,
        id_grupo = nuevo_grupo.id

    )

    db.add(nuevo_usuario_grupo)



def generate_base_albums(db):
    

    nuevo_album = Albums(
        name="nuevo_test"
    )
    db.add(nuevo_album)
    db.commit()
    

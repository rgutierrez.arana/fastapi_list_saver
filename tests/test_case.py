import sys
from typing import Tuple
sys.path.append("../..")
from fastapi.testclient import TestClient


from app.main import app

from tests.fixtures.user_fixtures import test_app,create_test_database,generate_initial_data,shared_user
from tests.fixtures.user_fixtures import UserContainer
import pytest

#!ALBUM SECTION


def test_list_album(shared_user: Tuple[UserContainer, TestClient]):

    user, client = shared_user
    headers = {"Authorization": f"Bearer {user.access_token}"}

    resp = client.get("/api/v1.0/albums?item_pagina=10&pag_actual=1",
                      headers=headers
                      )

    assert resp.status_code == 200, f"No se pudo obtener data {resp.content}"

    return True


def test_create_detail_album(shared_user: Tuple[UserContainer, TestClient]):


    user, client = shared_user
    headers = {"Authorization": f"Bearer {user.access_token}"}
    
    json_data = {"name": "lovers in the air"}

    resp = client.post("/api/v1.0/albums", headers=headers, json=json_data)

    nuevo_album = resp.json()

    resp = client.get(f"/api/v1.0/albums/{nuevo_album['id']}",
                      headers=headers)

    data_json = resp.json()

    assert resp.status_code == 200, f"No se pudo obtener detalle album data \n{resp.content}"
    assert type (data_json ) ==dict ,f"El tipo de dato actual no es dict {type(data_json) }"

    return True



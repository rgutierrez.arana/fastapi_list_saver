import websocket
import threading as th
import time
import requests as req
import json
def on_message(ws, message):
    print("MESSAGE : ", message )
    
def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")

def on_open(ws):
    data = {"action" : "SERVER_TEST" , "msg" : "mensaje de prueba"}

    str_data = json.dumps(data)
    ws.send(str_data)
def enviar_mensaje_server(ws):

    while True:
        data = {"action": "SERVER_TEST", "msg": "mensaje de prueba"}
        texto_entrada = input("Ingresa lo que quieres enviar : ")
        data["msg"] = texto_entrada
        str_data = json.dumps(data)
        ws.send(str_data)


if __name__ == "__main__":
    #ws://localhost:8000/api/v1.0/device
    websocket.enableTrace(False)
    ruta_ws = "ws://localhost:8000/api/v1.0/ws/device"
    ruta_login = "http://localhost:8000/api/v1.0/auth/token"
    apikey = "0ec0338349ec376a2d02753199574ef107ee6b587ff078907839786e034ae7ed"
    
    access_data = {"grant_type": "apikey", "apikey": apikey}
    acceso = req.post(url=ruta_login,data=access_data)
    
    if acceso.status_code== 200:
        tokens=acceso.json()
        user_id = tokens["id"]
        data_cookie_acceso = f"cookie_jwt={tokens['access_token']}"

        ws = websocket.WebSocketApp(ruta_ws,
                                    on_message=on_message,
                                    on_error=on_error,
                                    on_close=on_close,
                                    cookie=data_cookie_acceso)    
        ws.on_open = on_open

        # thread_actual=th.Thread(target=enviar_mensaje_server,
        #           name="socket_servido", 
        #           kwargs={"ws": ws})

        #thread_actual.start()
        ws.run_forever()
        
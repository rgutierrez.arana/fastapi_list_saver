import base64
import hashlib
import calendar
import datetime

secret = "itsaSSEEECRET"
url = "Biblioteca/test.txt"

future = datetime.datetime.utcnow() + datetime.timedelta(minutes=1)
expiry = calendar.timegm(future.timetuple())

secure_link = "{key}{expiry}".format(key=secret,
                                          #url=url,
                                          expiry=expiry)


hash = hashlib.md5(secure_link.encode("utf-8")).digest()
encoded_hash = base64.urlsafe_b64encode(hash).decode("utf-8").rstrip('=')

print( url + "?st=" + encoded_hash + "&e=" + str(expiry)  )


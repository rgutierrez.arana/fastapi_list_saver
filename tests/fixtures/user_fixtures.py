import pytest
from fastapi.testclient import TestClient
from app.main import app
from sqlalchemy_utils import create_database, database_exists, drop_database
from core.extensions import base, get_db
from tests.db_utils import override_get_db, SQLALCHEMY_DATABASE_URL, create_engine
from initial_data.initial_data_user import generate_base_permisions, generate_admin_user

@pytest.fixture(scope="session", )
def test_app() -> TestClient:
    app.dependency_overrides[get_db] = override_get_db
    client = TestClient(app)

    yield client  # testing happens here


@pytest.fixture(
    #scope="session",  #!INDICA QUE LO VA A EJECUTAR SOLAMENTE 1 VEZ EN TODO EL MODULO
    #autouse=True
)
def create_test_database():
    print("creando DB")
    url = SQLALCHEMY_DATABASE_URL
    engine = create_engine(url)
    if database_exists(url):
        drop_database(url)
    create_database(url)  # Create the test database.
    base.metadata.create_all(engine)  # Create the tables.

    yield override_get_db()  # Run the tests.
    print("Destruyendo  DB")
    drop_database(url)  # Drop the test database.


@pytest.fixture()
def generate_initial_data(create_test_database):
    db = next(create_test_database)
    generate_base_permisions(db)
    generate_admin_user(db)

    db.commit()


class UserContainer():
    def __init__(self,
                 id: int=None,
                 access_token: str=None,
                 token_type: str=None,
                 refresh_token: str=None,
                 correo: str=None,
                 nombre_completo: str=None) -> None:

        self.id: int = id
        self.access_token: str = access_token
        self.token_type: str = token_type
        self.refresh_token: str = refresh_token
        self.correo: str = correo
        self.nombre_completo: str = nombre_completo
    def __repr__(self) -> str:
        return f"""
            id = {self.id}
            access_token = {self.access_token}
            token_type = {self.token_type}
            refresh_token = {self.refresh_token}
            correo = {self.correo}
            nombre_completo = {self.nombre_completo}
        """

    



@pytest.fixture()
def shared_user(generate_initial_data , test_app:TestClient):
    data= {
        "grant_type" : "password",
        "username" :"admin",
        "password":"admin"
    }
    result = test_app.post(
        "/api/v1.0/auth/token",
        headers={"Content-Type": "application/x-www-form-urlencoded"},
        data=data
    )

    user_data = result.json()


    current_user = UserContainer(**user_data)    
    return current_user , test_app

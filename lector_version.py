import sys

archivo = sys.argv[1]
base_str= "    op.execute(CreateSequence(Sequence('%s')))"
with open(archivo ,"r") as documento:
    print ( "    from sqlalchemy.schema import Sequence, CreateSequence")
    for linea in documento.readlines():
        if "sa.text('" in linea:
            temp = linea.split("sa.text('")[-1].split("')")[0].replace(".nextval","")
            print ( base_str%(temp) ) 
FROM python:3.8

ENV LD_LIBRARY_PATH="/opt/oracle/instantclient_19_6:${LD_LIBRARY_PATH}"

COPY requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

WORKDIR /app/
COPY ./app /app
ENV PYTHONPATH=/app
